import os

import numpy as np
import resampy
import tensorflow as tf
from tensorflow.keras import Model
from tensorflow.keras import layers
from tensorflow.keras import optimizers

BASE_DIR = os.path.dirname(__file__)

BATCH_SIZE = 32
SAMPLE_RATE = 44100
SLICE_NUM_SAMPLES = 22050
RMS_THRESHOLD = 0.04

CLASS_LOSSLESS = 0
CLASS_LOSSY = 1
CLASS_QUIET = 2
CLASS_NAMES = ['lossless', 'lossy']


def preprocess_step(frame_length, frame_step, data):
    t_casted = tf.cast(data, tf.float32)
    t_transp = tf.transpose(t_casted, [0, 2, 1])
    t_stft = tf.signal.stft(
        signals=t_transp,
        frame_length=frame_length,
        frame_step=frame_step,
    )
    t_real = tf.math.real(t_stft)
    t_abs = tf.math.abs(t_real)
    t_log = tf.math.log(t_abs + 0.001)
    t_retransp = tf.transpose(t_log, [0, 3, 2, 1])
    return t_retransp


def preprocess_small(data):
    return preprocess_step(512, 256, data)


def preprocess_large(data):
    return preprocess_step(1024, 512, data)


def get_model():
    t_input = layers.Input(shape=(SLICE_NUM_SAMPLES, 2))
    x = layers.Lambda(preprocess_small)(t_input)
    y = layers.Lambda(preprocess_large)(t_input)

    x = layers.Conv2D(16, 3, activation='relu', padding='same')(x)
    x = layers.MaxPool2D(2)(x)

    y = layers.Conv2D(16, 3, activation='relu', padding='same')(y)
    y = layers.MaxPool2D(2)(y)

    x = layers.Conv2D(32, 3, activation='relu', padding='same')(x)
    x = layers.MaxPool2D(2)(x)

    y = layers.Conv2D(32, 3, activation='relu', padding='same')(y)
    y = layers.MaxPool2D(2)(y)

    x = layers.Conv2D(64, 3, activation='relu', padding='same')(x)
    x = layers.MaxPool2D(2)(x)

    y = layers.Conv2D(64, 3, activation='relu', padding='same')(y)
    y = layers.MaxPool2D(2)(y)

    x = layers.Conv2D(128, 3, activation='relu', padding='same')(x)
    x = layers.MaxPool2D(2)(x)

    y = layers.Conv2D(128, 3, activation='relu', padding='same')(y)
    y = layers.MaxPool2D(2)(y)

    x = layers.Flatten()(x)
    x = layers.Dropout(0.2)(x)

    y = layers.Flatten()(y)
    y = layers.Dropout(0.2)(y)

    x = layers.Concatenate(axis=1)([x, y])

    x = layers.Dense(128, activation='relu')(x)
    x = layers.Dropout(0.2)(x)

    output = layers.Dense(3, activation='softmax')(x)

    model = Model(t_input, output)
    model.compile(
        loss='categorical_crossentropy',
        optimizer=optimizers.SGD(),
        # optimizer=optimizers.Adam(),
        metrics=['acc'],
    )
    return model


def cut_audio(data, samplerate, num_pieces):
    num_samples = data.shape[0]
    step = max(SLICE_NUM_SAMPLES, num_samples // num_pieces)
    result = []
    for start in range(0, num_samples - SLICE_NUM_SAMPLES + 1, step):
        cut_data = data[start:start + SLICE_NUM_SAMPLES]
        assert len(cut_data) == SLICE_NUM_SAMPLES
        result.append(cut_data)
    return result


def get_rms(data):
    return max(
        np.std(data[:, 0]),
        np.std(data[:, 1]),
    )


def ensure_redbook(data, samplerate):
    if samplerate == SAMPLE_RATE:
        return data, SAMPLE_RATE
    else:
        return resampy.resample(data, samplerate, SAMPLE_RATE, axis=0), SAMPLE_RATE

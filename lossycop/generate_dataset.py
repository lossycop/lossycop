#!/usr/bin/env python3

import argparse
import multiprocessing
import os
import random
import subprocess
import sys

import soundfile

from lossycop.common import cut_audio, ensure_redbook, BASE_DIR

TRAIN_SPLIT = 0.85
SCRATCH_DIR = os.path.join(BASE_DIR, 'scratch')

NUM_PIECES_LOSSY = 5

LAME_OPTIONS_LIST = {
    '32': ['-b', '32'],
    '64': ['-b', '64'],
    '96': ['-b', '96'],
    '128': ['-b', '128'],
    '160': ['-b', '160'],
    '192': ['-b', '192'],
    '256': ['-b', '256'],
    '320': ['-b', '320'],
    'v9': ['-V', '9'],
    'v7': ['-V', '7'],
    'v6': ['-V', '6'],
    'v5': ['-V', '5'],
    'v4': ['-V', '4'],
    'v3': ['-V', '3'],
    'v2': ['-V', '2'],
    'v1': ['-V', '1'],
    'v0': ['-V', '0'],
}


def suffix_filename(filename, suffix, new_ext=None):
    base, ext = os.path.splitext(filename)
    return '{}_{}{}'.format(base, suffix, new_ext or ext)


def execute_chain(chain):
    processes = []
    stdin = None
    for options in chain:
        p = subprocess.Popen(options, stdin=stdin, stdout=subprocess.PIPE)
        stdin = p.stdout
        processes.append(p)

    stdout, _ = processes[-1].communicate()
    return stdout


def lame_roundtrip(src_path, options):
    name = str(random.randint(0, sys.maxsize))
    mp3 = os.path.join(SCRATCH_DIR, name + '.mp3')
    wav = os.path.join(SCRATCH_DIR, name + '.wav')
    subprocess.check_call(['flac', '--silent', '--decode', '--output-name', wav, src_path])
    subprocess.check_call(['lame', '--quiet'] + options + [wav, mp3])
    subprocess.check_output(['lame', '--quiet', '--decode', '--mp3input', mp3, wav])
    result = soundfile.read(wav)
    os.remove(mp3)
    os.remove(wav)
    return result


def write_cuts(dst_dir, src_filename, cuts, samplerate):
    os.makedirs(dst_dir, exist_ok=True)
    for i, data in enumerate(cuts):
        dst_filename = suffix_filename(os.path.splitext(src_filename)[0], '{:02d}'.format(i), '.wav')
        dst_path = os.path.join(dst_dir, dst_filename)
        print('Writing', dst_path)
        soundfile.write(dst_path, data, samplerate)


def add_file(args):
    dst_path, file_path = args
    base_name = os.path.basename(file_path)
    print('Generating', base_name, 'lossless')
    lossless_data, lossless_samplerate = ensure_redbook(*soundfile.read(file_path))
    lossless_cuts = cut_audio(lossless_data, lossless_samplerate, NUM_PIECES_LOSSY * len(LAME_OPTIONS_LIST))
    write_cuts(
        os.path.join(dst_path, 'lossless'),
        os.path.basename(file_path),
        lossless_cuts,
        lossless_samplerate,
    )

    for lame_name, lame_options in LAME_OPTIONS_LIST.items():
        print('Generating', base_name, lame_name)
        lossy_data, lossy_samplerate = ensure_redbook(*lame_roundtrip(file_path, lame_options))
        lossy_cuts = cut_audio(lossy_data, lossy_samplerate, NUM_PIECES_LOSSY)
        write_cuts(
            os.path.join(dst_path, 'lossy'),
            suffix_filename(base_name, lame_name),
            lossy_cuts,
            lossy_samplerate,
        )


def split_train_validation(src_path, dst_path, max_files=None):
    files = [os.path.join(src_path, f) for f in os.listdir(src_path)]
    random.shuffle(files)

    if max_files:
        files = files[:max_files]
    split_idx = int(len(files) * TRAIN_SPLIT)
    train_files = files[:split_idx]
    validation_files = files[split_idx:]
    train_path = os.path.join(dst_path, 'train')
    validation_path = os.path.join(dst_path, 'validation')
    train_jobs = [(train_path, f) for f in train_files]
    validation_jobs = [(validation_path, f) for f in validation_files]
    pool = multiprocessing.Pool(os.cpu_count())
    pool.map(add_file, train_jobs + validation_jobs, chunksize=1)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('src_path')
    parser.add_argument('dst_path')
    parser.add_argument('--max-files', default=None, type=int)
    args = parser.parse_args()
    split_train_validation(args.src_path, args.dst_path, args.max_files)


if __name__ == '__main__':
    main()
